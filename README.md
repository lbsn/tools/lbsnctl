# lbsnctl - control your LBSN environment

This script aims to be a simple tool to kickstart the [LBSN] environment. It
wraps the [Git] and [Docker] command line interfaces with a few easy commands.


## Commands

    download  downloads all repositories into subdirectories: `git clone`
    update    updates all repositories including submodules: `git pull`
    start     starts all services: `docker-compose up`
    stop      stops all services: `docker-compose down`
    import    load data into a database: `lbsntransform`
    export    archive database directories into tarballs: `tar -cjf`
    re-import extract archived tarballs into a database: `tar -xjf`
    delete    deletes all cloned repositories: `rm -rf`
    status    shows config, envvars, repos, containers: `printf`, `docker ps`


## Software Requirements

* [Git]
* [Docker]
* [docker-compose]


## Installation

You can just download the script, but for now it is recommended to install it using Git:

    cd /your/prefered/script-location
    git clone https://gitlab.vgiscience.de/lbsn/tools/lbsnctl.git .

You should add this script to your list of executable programs. This can be done e.g. like this:

    ln -s $(pwd)/lbsnctl.sh /usr/local/bin/lbsnctl

Then you can run the script this way:

    lbsnctl status

For the rest of the README it is assumed to be done like that.


## Settings

Configuration of the script takes place through environment variables:

| Environment variables | Explanation                                     |
| --------------------- | ----------------------------------------------- |
| `REPOS`               | Array of paths to the repositories              |
| `FORGE`               | Domain of the software forge, e.g. GitLab       |
| `NETWORK_NAME`        | Docker network name                             |
| `TARBALL_DIR`         | Directory to store exported tarballs            |
| `CLONE_DIR`           | Clone directory for the repositories            |
| `REMOTE`              | Name of the Git remote                          |
| `BRANCH`              | Git branch to download/clone                    |
| `SSH`                 | Set true if cloning via SSH is preferred        |
| `POSTGRES_PASSWORD`   | Password of the postgres user                   |
| `PGADMIN_PASSWORD`    | Password to the pgadmin account                 |
| `PGADMIN_WEBURL`      | (external) URL for the pgAdmin web interface    |
| `JUPYTER_WEBURL`      | (external) URL for the JupyterLab web interface |

These environment variables can be set in a configuration file `lbsnctl.conf`. This should be done, **before** the `download` command. See [lbsnctl.conf.example] for an example of how this file can look like. It can be placed globally in `/etc/lbsnctl.conf`, each system user's configuration directory at `$HOME/.config/lbsnctl.conf` and in the script's directory itself (`./`). The script will search for configuration files in that order, so it is possible to override individual settings. If no configuration file is found, the script will fall back to [default values].


### download

    lbsnctl download

Downloads these tools from repositories using [Git]:

* [rawdb]: Postgres database server for the raw data
* [hlldb]: Postgres database for the HyperLogLog data
* [pgadmin]: Browser-based database administration frontend
* [jupyterlab]: Browser-based environment for interactive and reproducible computing

The script tries to download/clone via HTTPS by default. If you do have set up a valid SSH connection to the repository and want to use it instead, add `SSH=true` to `lbsnctl.conf`.

If there is a file `servers.json` within the cloned directory (which is the case for the [pgadmin] repository), and if there is also a file `servers.json` within this working directory, the `download` command will copy it from here to there. You should rename or copy `servers.json.example` to `servers.json`.

The `REMOTE` variable tells Git how to locally name the remote repository. If not set, it will default to `origin`.

The `BRANCH` variable tells Git which branch to clone. If not set, it will default to `master`.

The `CLONE_DIR` variable is to set the destination for the cloned repositories. It will default to the current working directory (`.`). If you have a certain directory for your programming projects, you should set that as the working directory. Setting e.g. `CLONE_DIR="$HOME/projects"` will create a structure like this in your home directory:

    ~/projects/
    └── lbsn
        ├── databases
        │  ├── hlldb
        │  │  ├── init
        │  │  └── structure
        │  │      └── structure
        │  └── rawdb
        │      ├── init
        │      └── structure
        │          └── structure
        └── tools
            └── pgadmin
            └── jupyterlab


### update

    lbsnctl update

Updates all repositories including their submodules using [Git].


### start

    lbsnctl start

Starts all the containers using `docker-compose up`. All the tools connect to an [external] Docker network, that can be configured through the `NETWORK_NAME` environment variable. If this network does not exist, it will be created by this script prior to bringing the containers up.

When the `-c` or `--credentials` option is passed, it will print a link and the login credentials to the command line.


### stop

    lbsnctl stop

Stops all the containers using `docker-compose down`. The external Docker network configured through the `NETWORK_NAME` will be kept.


### import

    lbsnctl import [<directory>/<url>] [<lbsntransform parameters>]

Imports initial data into both databases, if exist, from CSV, either passed as a URL or a local directory containing one or more `.csv` files.

If neither is passed, a default [demo dataset] is used, which is a subset of the [yfcc100m] dataset consisting of 10k items, about 5MB in size. This dataset is only good for testing or very basic showcase purposes.

The import uses a Docker image of [lbsntransform] to perform the data conversion. Per default, it creates only `post` objects and `community,hashtag` bases for the HLL import. This can be altered by passing appropriate `lbsntransform` arguments to this command.


### export

    lbsnctl export

Archives each of the database directories `/var/lib/postgresql/data` into a bzip2-compressed tarball. They will be stored in the current working directory by default, which can be changed by setting the `TARBALL_DIR` environment variable.

Exporting a database directory manually can be done e.g. like this (assuming `lbsn-hlldb` the name of the database container):

    docker run \
        --rm \
        --volumes-from lbsn-hlldb \
        --volume $(pwd):/export \
        busybox \
        sh -c "cd /var/lib/postgresql/data && tar -cjf /export/$(date +%Y%m%d)_lbsn-hlldb.tar.bz2 ."

This creates a temporary [busybox] container and attaches the [internal volume][1] created by the database container to it using `--volumes-from lbsn-hlldb`. The executed command creates a tarball from the database directory inside the container and stores it in the second volume, which is the current working directory on the host (`$(pwd)`).


### re-import

    lbsnctl re-import

For each database repository, this creates a [docker volume][2] and extracts the tarball into it. By now, only bzip2-compressed tarballs can be imported this way. See [export] for how to create those.

Valid importable data is a copy of a `/var/lib/postgresql/data` directory. They can also be created differently and then imported manually with other commands than `tar -xjf`, see below.

**Important:** Containers must not run at the time of the import. Use `lbsnctl stop` to stop them first.

Manual creation of a volume can be done e.g. like this:

    docker volume create lbsn-hlldb
    docker run \
        --rm \
        --volume "lbsn-hlldb:/data" \
        --volume "$(pwd)/lbsn-hlldb.tar.bz2:/import.tar.bz2" \
        busybox \
        sh -c "cd /data && tar -xjf /import.tar.bz2"

This creates a [docker volume][2] with the name `lbsn-hlldb` and a temporary [busybox] container, that mounts that volume as well as the file `lbsn-hlldb.tar.bz2`. The executed command extracts that tarball into the volume.



### delete

    lbsnctl delete

Deletes all the repositories configured in `REPOS` environment variable array and removes the external Docker network configured through the `NETWORK_NAME` environment variable. There will be *one* confirmation prompt before the deletion.


### status

    lbsnctl status

Shows the status of the project, including all found configuration files, environment variables and running Docker containers.


[LBSN]: https://lbsn.vgiscience.org
[Git]: https://git-scm.com
[Docker]: https://www.docker.com
[docker-compose]: https://docs.docker.com/compose/
[rawdb]: https://gitlab.vgiscience.de/lbsn/databases/rawdb
[hlldb]: https://gitlab.vgiscience.de/lbsn/databases/hlldb
[pgadmin]: https://gitlab.vgiscience.de/lbsn/tools/pgadmin
[jupyterlab]: https://gitlab.vgiscience.de/lbsn/tools/jupyterlab
[external]: https://docs.docker.com/compose/networking/#use-a-pre-existing-network
[busybox]: https://hub.docker.com/_/busybox
[1]: https://github.com/docker-library/postgres/blob/0d0485cb02e526f5/11/Dockerfile#L169
[2]: https://docs.docker.com/storage/volumes
[lbsnctl.conf.example]: https://gitlab.vgiscience.de/lbsn/tools/lbsnctl/blob/master/lbsnctl.conf.example
[clone]: #clone
[update]: #update
[start]: #start
[stop]: #stop
[export]: #export
[import]: #import
[delete]: #delete
[status]: #status
[default values]: https://gitlab.vgiscience.de/lbsn/tools/lbsnctl/blob/master/lbsnctl.sh#L395-407
[demo dataset]: https://kartographie.geo.tu-dresden.de/downloads/lbsn/flickr_yfcc100m/shuf-n10000_dataset.csv
[yfcc100m]: https://multimediacommons.wordpress.com/yfcc100m-core-dataset/
[lbsntransform]: https://gitlab.vgiscience.de/lbsn/lbsntransform

#!/usr/bin/env bash

set -e

usage() {

    readme="$script_path/README.md"

    [[ "$1" == "more" ]] && {
        less "$readme"
        exit 0
    }

    echo
    grep -B 6 -A 9 "## Commands" "$readme"

    printf "\nFor more details, run %s help more\n" "$0"

}

# help functions for user interaction. `xargs` to trim white space caused by \
#     such line breaks within string parameters
notify_user() {
    echo
    printf " %b\n" "$(echo "$@" | xargs)"
    echo
}
ask_user() {
    echo
    printf " [?] %b " "$(echo "$@" | xargs)"
}
exit_user() {
    echo
    printf " [X] %b\n" "$(echo "$@" | xargs)"
    echo
    exit 1
}

################################################################################
#
#   ##   #  # # ### #    #   #  ##
#   # # # # # # # # #   # # # # # #
#   # # # # ### # # #   # # ### # #
#   # # # # ### # # #   # # # # # #
#   ##   #  # # # # ###  #  # # ##
#
#   downloads all repositories into subdirectories: `git clone`
#

do_clone() {

    if [[ $SSH == true ]]; then

        PROTOCOL="git@${FORGE}:"

    else

        # cache https credentials so it will only ask once
        git config --global credential.helper cache

        PROTOCOL="https://${FORGE}/"

    fi

    # generate random passwords, if not set previously
    [[ -z ${POSTGRES_PASSWORD} ]] &&
        POSTGRES_PASSWORD=$(openssl rand -base64 21)
    [[ -z ${PGADMIN_PASSWORD} ]] &&
        PGADMIN_PASSWORD=$(openssl rand -base64 21)

    # clone the repos
    for REPO in "${REPOS[@]}"; do

        # clone this repo if it does not yet exist in CLONE_DIR
        [[ ! -d "$CLONE_DIR/$REPO" ]] &&
            git clone --origin "${REMOTE}" \
                --single-branch --branch "${BRANCH}" \
                --recurse-submodules \
                "${PROTOCOL}${REPO}.git" "${CLONE_DIR}/${REPO}"

        HOME=$(pwd) && cd "${CLONE_DIR}/$REPO/"

        cp ".env.example" ".env"

        # override example variables in .env file with previously set ones

        override_env_vars=(
            "NETWORK_NAME"
            "POSTGRES_PASSWORD"
            "PGADMIN_PASSWORD"
            "PGADMIN_WEBURL"
            "PGADMIN_WEBPORT"
            "JUPYTER_NOTEBOOKS"
            "JUPYTER_PASSWORD"
            "JUPYTER_WEBURL"
            "JUPYTER_WEBPORT"
        )

        for env_var in "${override_env_vars[@]}"; do

            # test if this variable is set
            if [[ -n ${!env_var} ]]; then
                # uncomment any commented variables
                sed -i "/$env_var/s/^#\s*//g" .env
                # replace original values with new ones
                sed -ri "s|^($env_var\s*=\s*).*|\1${!env_var}|" .env
            fi

        done

        cd "$HOME"

        # copy pgadmin lbsn-server configuration if pgadmin repo
        [[ -f "${CLONE_DIR}/${REPO}/servers.json.example" ]] &&
            cp "$script_path/servers.json" "${CLONE_DIR}/${REPO}/servers.json"

        echo

    done

}

################################################################################
#
#   # # ##  ##   #  ### ###
#   # # # # # # # #  #  #
#   # # ##  # # ###  #  ##
#   # # #   # # # #  #  #
#   ### #   ##  # #  #  ###
#
#   updates all repositories including submodules: `git pull`
#

do_update() {

    # update the repos
    for REPO in "${REPOS[@]}"; do

        HOME=$(pwd) && cd "${CLONE_DIR}/$REPO/"

        notify_user "updating $REPO"
        git checkout "${BRANCH}"
        git pull --recurse-submodules "${REMOTE}" "${BRANCH}"

        cd "$HOME"

    done

}

################################################################################
#
#    ## ###  #  ##  ###
#   #    #  # # # #  #
#    #   #  ### ##   #
#     #  #  # # # #  #
#   ##   #  # # # #  #
#
#   starts all services: `docker-compose up`
#

compose_up() {

    # check if docker network is available or create it
    docker network inspect "${NETWORK_NAME}" &>/dev/null || {
        echo -n "Creating network "
        docker network create "${NETWORK_NAME}"
    }

    case "$1" in
    -c | --cred | --credentials)
        SHOW=true
        ;;
    esac

    # start the containers
    for REPO in "${REPOS[@]}"; do

        # change to the repo directory
        HOME=$(pwd) && cd "${CLONE_DIR}/$REPO/"
        SERVICE=$(docker-compose config --services)

        if [[ $SERVICE == "pgadmin" ]]; then

            # pgadmin

            # add config to include default servers to docker-compose
            DCSY="docker-compose.servers.yml"
            # only if DCSY is not already added (relevant on a second "up")
            sed -i "/^COMPOSE_FILE/{ /$DCSY/! s/$/:$DCSY/; };" .env

            docker-compose up --detach

            if [[ $SHOW == true ]]; then

                # get pgadmin login credentials from config
                CONFIG=$(docker-compose config | tr -d " ")
                PGADMIN_MAIL=$(echo "$CONFIG" |
                    grep PGADMIN_DEFAULT_EMAIL | cut -f 2 -d ':')
                PGADMIN_PASSWORD=$(echo "$CONFIG" |
                    grep PGADMIN_DEFAULT_PASSWORD | cut -f 2 -d ':')

                # if no weburl is set, get the port and create localhost url
                [[ -z $PGADMIN_WEBURL ]] &&
                    PGADMIN_WEBPORT=$(echo "$CONFIG" | grep -A 1 ports |
                        tail -n 1 | cut -f 2 -d : | tr -d "\n") &&
                    PGADMIN_WEBURL="http://127.0.0.1:$PGADMIN_WEBPORT"

                tabs 4
                echo
                printf "\t%b\n" \
                    "pgAdmin: \t $PGADMIN_WEBURL" \
                    "Login: \t\t $PGADMIN_MAIL" \
                    "Password:\t $PGADMIN_PASSWORD"

            fi

        elif [[ $SERVICE == "jupyterlab" ]]; then

            # jupyterlab

            docker-compose up --detach

            if [[ $SHOW == true ]]; then

                # get pgadmin login credentials from config
                CONFIG=$(docker-compose config | tr -d " ")
                JUPYTER_PASSWORD=$(echo "$CONFIG" |
                    grep JUPYTER_PASSWORD | cut -f 2 -d ':')
                JUPYTER_WEBURL=$(echo "$CONFIG" |
                    grep JUPYTER_WEBURL | cut -d ':' -f 2,3)
                tabs 4
                echo
                printf "\t%b\n" \
                    "JupyterLab:\t $JUPYTER_WEBURL" \
                    "Password: \t $JUPYTER_PASSWORD"

            fi

        else

            # databases

            if [[ -f "docker-compose.yml" ]]; then
                docker-compose up --detach
            fi

        fi

        cd "$HOME"
        echo

    done

}

################################################################################
#
#    ## ###  #  ##
#   #    #  # # # #
#    #   #  # # ##
#     #  #  # # #
#   ##   #   #  #
#
#   stops all services: `docker-compose down`
#

compose_down() {

    # bring the repos down
    for REPO in "${REPOS[@]}"; do

        HOME=$(pwd) && cd "${CLONE_DIR}/$REPO/"

        docker-compose down

        cd "$HOME"

    done

}

################################################################################
#
#   ### # # ##   #  ##  ###
#    #  ### # # # # # #  #
#    #  ### ##  # # ##   #
#    #  # # #   # # # #  #
#   ### # # #    #  # #  #
#
#   load data into a database: `lbsntransform`
#

do_import() {

    # docker image to import the data
    lbsntransform="registry.gitlab.vgiscience.org/lbsn/lbsntransform:latest"

    # default arguments for docker run
    docker_args=(
        --rm
        --network "$NETWORK_NAME"
    )

    # default arguments for lbsntransform
    lbsntransform_args=(
        --origin 21
        --file_input
        --file_type "csv"
        --csv_delimiter $'\t'
        --mappings_path ./resources/mappings/
    )

    # set input path or url
    if [[ -n $1 ]]; then

        # if first arg is a file, replace its value with enclosing directory
        if [[ -f $1 ]]; then
            set -- "$(dirname "$1")" "${@:2}"
        fi

        # test if first argument is a directory
        if [[ -d $1 ]]; then
            docker_args+=(
                --volume "$1:$1"
            )
            # zip records if there is more than one csv file
            num_csv=$(find "$1" -maxdepth 1 -iname "*.csv" | wc -l)
            if [[ $num_csv -gt 1 ]]; then
                lbsntransform_args+=(
                    --zip_records
                )
            fi
            lbsntransform_args+=(
                --input_path_url "$1"
            )

            # remove first argument for further custom args
            shift

        fi
    else
        # use default demo dataset
        lbsntransform_args+=(
            --input_path_url "$(echo "https://kartographie.geo.tu-dresden.de/ \
                downloads/lbsn/flickr_yfcc100m/ \
                shuf-n10000_dataset.csv" | tr -d " ")"
        )
    fi

    # any remaining arguments can be used to override/extend default arguments
    custom_args=("$@")

    echo -ne "\n Pulling lbsntransform image " &&
        docker pull --quiet "$lbsntransform"
    echo

    for REPO in "${REPOS[@]}"; do

        # change to the repo directory
        HOME=$(pwd) && cd "${CLONE_DIR}/$REPO/"

        # get configuration of the docker container
        CONFIG=$(docker-compose config | tr -d " ")

        # get name of the database
        DATABASE_NAME=$(echo "$CONFIG" |
            grep DATABASE_NAME | cut -f 2 -d ':')

        # run the import only if this is a database
        if [[ -n $DATABASE_NAME ]]; then

            # get name of docker container
            CONTAINER_NAME=$(echo "$CONFIG" |
                grep container_name | cut -f 2 -d ':')

            # get database password
            POSTGRES_PASSWORD=$(echo "$CONFIG" |
                grep POSTGRES_PASSWORD | cut -f 2 -d ':')

            lbsntransform_args+=(
                --dbserveraddress_output "$CONTAINER_NAME:5432"
                --dbname_output "$DATABASE_NAME"
                --dbuser_output "postgres"
                --dbpassword_output "$POSTGRES_PASSWORD"
            )

            # add hll-specific arguments to the array
            [[ $DATABASE_NAME =~ "hll" ]] &&
                lbsntransform_args+=(
                    --dbformat_output "hll"
                    --dbserveraddress_hllworker "$CONTAINER_NAME:5432"
                    --dbname_hllworker "$DATABASE_NAME"
                    --dbuser_hllworker "postgres"
                    --dbpassword_hllworker "$POSTGRES_PASSWORD"
                    --include_lbsn_objects "post"
                    --include_lbsn_bases "community,hashtag"
                )

            # do the import
            docker run \
                "${docker_args[@]}" \
                "$lbsntransform" \
                "${lbsntransform_args[@]}" \
                "${custom_args[@]}"
        fi

    done

}

################################################################################
#
#   ### # # ##   #  ##  ###
#   #   # # # # # # # #  #
#   ##   #  ##  # # ##   #
#   #   # # #   # # # #  #
#   ### # # #    #  # #  #
#
#   archive database directories into tarballs: `tar -cjf`
#

do_export() {

    notify_user "Pulling temporary image to run the export in" &&
        docker pull busybox:latest

    # export data from running database containers
    for REPO in "${REPOS[@]}"; do

        # change to the repo directory
        HOME=$(pwd) && cd "${CLONE_DIR}/$REPO/"

        # get configuration of the docker container
        CONFIG=$(docker-compose config | tr -d " ")

        # get name of docker container
        CONTAINER_NAME=$(echo "$CONFIG" | grep container_name | cut -f 2 -d ':')

        # get name of the database
        DATABASE_NAME=$(echo "$CONFIG" | grep DATABASE_NAME | cut -f 2 -d ':')

        # if database name exists, it is a database repo. export the database
        if [[ -n $DATABASE_NAME ]] && [[ -n $CONTAINER_NAME ]]; then

            # ask whether this database should be exported
            ask_user "Do you want to export database $DATABASE_NAME? [y/N]"
            read -r EXPORT_THIS

            # if not yes, abort this export and continue
            [[ $EXPORT_THIS != "y" ]] && continue

            notify_user "Exporting database $DATABASE_NAME from container \
                $CONTAINER_NAME. This may take a while…"

            # start a temporary container, mount the foreign volume and
            # archive the database directory into a bzip2-compressed tarball
            docker run \
                --rm \
                --name "$CONTAINER_NAME-export" \
                --volumes-from "$CONTAINER_NAME" \
                --volume "$TARBALL_DIR:/export" \
                busybox \
                sh -c "cd /var/lib/postgresql/data && tar -cjf \
                    /export/$(date +%Y%m%d)_$CONTAINER_NAME.tar.bz2 ."

        fi

        cd "$HOME"

    done
}

################################################################################
#
#   ##  ###     ### # # ##   #  ##  ###
#   # # #        #  ### # # # # # #  #
#   ##  ##  ###  #  ### ##  # # ##   #
#   # # #        #  # # #   # # # #  #
#   # # ###     ### # # #    #  # #  #
#
#   extract archived tarballs into a database: `tar -xjf`
#

do_reimport() {

    # check if docker network is available or create it
    docker network inspect "${NETWORK_NAME}" &>/dev/null ||
        docker network create "${NETWORK_NAME}"

    # look for files with tar, tar.gz, tar.bz2, … suffix in tarball dir
    notify_user "Available tarballs in TARBALL_DIR:"
    for TARBALL in "${TARBALL_DIR}"/*.tar*; do

        printf "\t%s\n" "$TARBALL"

    done

    # create array for re-imports to do
    declare -A REIMPORT_TODO
    declare -A PASSWORD_TODO

    for REPO in "${REPOS[@]}"; do

        # change to the repo directory
        HOME=$(pwd) && cd "${CLONE_DIR}/$REPO/"

        # get configuration of the docker container
        CONFIG=$(docker-compose config | tr -d " ")

        # stop re-import if this service is still running
        [[ $(docker-compose ps -q) != "" ]] &&
            exit_user "Some database containers are still running. \
                Run \'$(basename "$0") stop\' to stop them."

        # check if this is a database repo or skip to the next
        echo "$CONFIG" | grep DATABASE_NAME &>/dev/null || continue

        # get the volume name
        VOLUME_NAME=$(echo "$CONFIG" | grep -A 2 "^volumes:" |
            tail -n 1 | cut -f 2 -d : | tr -d " \n")

        # ask for the tarball to re-import into the volume
        ask_user "Enter full path of tarball to re-import into $VOLUME_NAME \
            (empty to skip):"
        read -r TARBALL_NAME

        # skip to next REPO if empty file name
        [[ $TARBALL_NAME == "" ]] && echo && continue

        # check if volume already exists
        docker volume inspect "$VOLUME_NAME" &>/dev/null && VOLUME_EXISTS=1

        # if volume exists, ask before overriding contents
        if [[ $VOLUME_EXISTS ]]; then
            ask_user "$VOLUME_NAME exists. Replace with $TARBALL_NAME? [y/N]"
            read -r REPLACE_VOLUME
            [[ $REPLACE_VOLUME != "y" ]] && echo && continue
            unset VOLUME_EXISTS
        else
            notify_user "Creating volume $VOLUME_NAME" &&
                docker volume create "$VOLUME_NAME" &>/dev/null
        fi

        # add this tarball to the re-import todo-list
        REIMPORT_TODO["$VOLUME_NAME"]="$TARBALL_NAME"

        # ask to update postgres password in the database
        ask_user "Replace postgres password with the one set in .env? [y/N]"
        read -r UPDATE_PW

        if [[ $UPDATE_PW == "y" ]]; then
            # if yes, remember the path to this docker-compose file
            # to come back here later to change the password
            PASSWORD_TODO["$VOLUME_NAME"]="${CLONE_DIR}/$REPO/"

        fi

        cd "$HOME"
        echo

    done

    # work through the re-import todo-list
    for VOLUME_NAME in "${!REIMPORT_TODO[@]}"; do

        notify_user "Re-importing ${REIMPORT_TODO[$VOLUME_NAME]} \
            into $VOLUME_NAME. This may take a while …"

        # start a temporary container, mount the created volume and
        # extract the bzip2-compressed tarball into it
        docker run \
            --rm \
            --name "$VOLUME_NAME-reimport" \
            --volume "$VOLUME_NAME:/data" \
            --volume "${REIMPORT_TODO[$VOLUME_NAME]}:/reimport.tar.bz2" \
            busybox \
            sh -c "cd /data && tar -xjf /reimport.tar.bz2"

        # if an entry for this volume name exists in the password todo-list
        if [[ -n ${PASSWORD_TODO[$VOLUME_NAME]} ]]; then

            # change to the repo directory
            HOME=$(pwd) && cd "${PASSWORD_TODO[$VOLUME_NAME]}"

            # get the password from .env
            POSTGRES_PASSWORD=$(grep POSTGRES_PASSWORD .env | cut -d = -f 2)
            # get the docker-compose service name
            SERVICE=$(docker-compose config --services)

            # start the contanier and run the password update query in psql
            docker-compose up --detach &>/dev/null
            docker-compose exec "$SERVICE" psql --username postgres --command \
                "ALTER USER postgres WITH PASSWORD '$POSTGRES_PASSWORD';" \
                &>/dev/null && notify_user "Password in $VOLUME_NAME replaced."
            docker-compose down &>/dev/null

            cd "$HOME"

        fi

    done

}

################################################################################
#
#   ##  ### #   ### ### ###
#   # # #   #   #    #  #
#   # # ##  #   ##   #  ##
#   # # #   #   #    #  #
#   ##  ### ### ###  #  ###
#
#   deletes all cloned repositories: `rm -rf`
#

do_delete() {

    # print directory tree to revise and prompt for confirmation
    printf "\n %s/\n" "${CLONE_DIR}"
    printf "   └─ %s\n" "${REPOS[@]}"
    ask_user "really delete these directories? [y/N]"
    read -r delete

    if [[ $delete == "y" ]]; then

        # remember existing volumes suspected to delete
        EXISTING_VOLUMES=()

        for REPO in "${REPOS[@]}"; do

            # change into the repo directory or continue if that fails
            HOME=$(pwd)
            cd "${CLONE_DIR}/$REPO/" || continue

            # get the volume name
            VOLUME_NAME=$(docker-compose config | grep -A 2 "^volumes:" |
                tail -n 1 | cut -f 2 -d : | tr -d " \n")

            # test if volume really exists and add to array if so
            docker volume inspect "$VOLUME_NAME" &>/dev/null &&
                EXISTING_VOLUMES+=("$VOLUME_NAME")

            # stop deletion if this service is still running
            [[ $(docker-compose ps -q) != "" ]] &&
                exit_user "Some containers are still running. \
                    Run \'$(basename "$0") stop\' to stop them."

            cd "$HOME"

            # remove directories if they are (still) there
            notify_user "deleting ${CLONE_DIR}/${REPO}"
            [[ -d "${CLONE_DIR}/${REPO}" ]] &&
                rm --recursive --dir --force "${CLONE_DIR:?}/${REPO}"

        done

        # remove leftover directories
        find "${CLONE_DIR}" -depth -mindepth 1 -type d -empty -delete

        # remove docker network if it exists
        docker network inspect "$NETWORK_NAME" &>/dev/null &&
            notify_user "removing docker network $NETWORK_NAME" &&
            docker network rm "$NETWORK_NAME" &>/dev/null

        # ask if existing volumes should also be removed
        if [[ ${#EXISTING_VOLUMES[@]} -gt 0 ]]; then

            for VOLUME_NAME in "${EXISTING_VOLUMES[@]}"; do
                echo "$VOLUME_NAME"
            done

            ask_user "also delete these volumes? [y/N]"
            read -r delete_volumes

            if [[ $delete_volumes == "y" ]]; then
                docker volume rm "${EXISTING_VOLUMES[@]}"
            fi
        fi
    fi
}

################################################################################
#
#    ## ###  #  ### # #  ##
#   #    #  # #  #  # # #
#    #   #  ###  #  # #  #
#     #  #  # #  #  # #   #
#   ##   #  # #  #  ### ##
#
#   shows config, envvars, repos, containers: `printf`, `docker ps`
#

show_status() {

    tabs 4

    [[ -n "${config_files[0]}" ]] && {
        echo -e "\nConfiguration files:"
        printf "\t%b\n" "${config_files[@]}"
    }

    echo -e "\nEnvironment variables:"
    printf "%b\n" \
        "::CLONE_DIR:$CLONE_DIR" \
        "::TARBALL_DIR:$TARBALL_DIR" \
        "::NETWORK_NAME:$NETWORK_NAME" \
        "::BRANCH:$BRANCH" \
        "::REMOTE:$REMOTE" \
        "::FORGE:$FORGE" \
        "::REPOS:${REPOS[*]}" | column -t -s ':'

    echo -e "\nCloned repositories:"
    for REPO in "${REPOS[@]}"; do
        [[ -d "${CLONE_DIR}/${REPO}" ]] &&
            printf "\t${CLONE_DIR}/%s\n" "${REPO}"
    done

    echo -e "\nRunning containers:"

    docker ps --filter "network=$NETWORK_NAME" --format \
        "table {{.Names}}\t{{.RunningFor}}\t{{.Status}}\t{{.Size}}\t{{.Ports}}"

    echo
}

################################################################################
#
#   ##  # # ###
#   # # # # # #
#   ##  # # # #
#   # # # # # #
#   # # ### # #
#

# check for required tools to be installed or exit
required_tools=(openssl git docker docker-compose grep awk less)
for tool in "${required_tools[@]}"; do
    command -v "$tool" >>/dev/null || exit_user "install $tool first"
done

# import variables from configuration files if available
script_path=$(dirname "$(readlink -f "$0")")

# if the file exists, source it and add path to array of known config files
source_and_remember() {
    if [[ -f "$1" ]]; then
        source "$1"
        config_files+=("$1")
    fi
}

source_and_remember "/etc/lbsnctl.conf"
source_and_remember "$HOME/.config/lbsnctl.conf"
source_and_remember "$script_path/lbsnctl.conf"

[[ -z "${config_files[0]}" ]] &&
    notify_user "No configuration files found. Using defaults."

# create remaining variables with default values if not previously set
default_repos=(
    "lbsn/databases/rawdb"
    "lbsn/databases/hlldb"
    "lbsn/tools/pgadmin"
    "lbsn/tools/jupyterlab"
)
REPOS=("${REPOS[@]:-"${default_repos[@]}"}")
: "${FORGE:="gitlab.vgiscience.de"}"
: "${NETWORK_NAME:="lbsn-network"}"
: "${TARBALL_DIR:="$(pwd)"}"
: "${CLONE_DIR:="$(pwd)"}"
: "${REMOTE:="origin"}"
: "${BRANCH:="master"}"

# remove command from argument list
COMMAND="$1"
shift

# run
case "$COMMAND" in
download | clone)
    do_clone
    ;;
update | pull)
    do_update
    ;;
start | up)
    compose_up "$@"
    ;;
stop | down)
    compose_down
    ;;
import)
    do_import "$@"
    ;;
export)
    do_export
    ;;
re-import | reimport)
    do_reimport
    ;;
delete | remove)
    do_delete
    ;;
status)
    show_status
    ;;
*)
    usage "$@"
    ;;
esac
